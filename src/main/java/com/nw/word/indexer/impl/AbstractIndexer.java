package com.nw.word.indexer.impl;

import java.io.IOException;
import java.util.Collection;

import org.apache.log4j.Logger;

import com.nw.word.indexer.Indexer;
import com.nw.word.reader.FileReader;

public abstract class AbstractIndexer implements Indexer {
	private static final Logger logger = Logger.getLogger(AbstractIndexer.class);
	private Class<? extends FileReader> readerImpl;
	
	@Override
	public void setFileReaderImpl(Class<? extends FileReader> readerImpl) {
		this.readerImpl = readerImpl;
	}
	
	protected void index(String file) {
		logger.debug("Indexing the file " + file);
		FileReader reader = null;
		
		try {
			reader = readerImpl.getDeclaredConstructor().newInstance();
			reader.setFileInputStreamIfNull(file);
			
			while (reader.hasNext()) {
				index(reader.nextChunkInWords());
			}
		} catch (Exception e) {
			logger.error("Error while indexing file: " + file);
			e.printStackTrace();
		} finally {
			closeQuitely(reader);
		}
	}

	private void closeQuitely(FileReader reader) {
		if (reader != null) {
			reader.close();
		}
	}
	
	public abstract void index(String[] nextChunkInWords);

	public abstract void index(Collection<String> files);
}
