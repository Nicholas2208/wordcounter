package com.nw.word.indexer;

import java.util.Collection;

import com.nw.word.reader.FileReader;

public interface Indexer {
	public void index(Collection<String> files);

	public int getWordCount(String word);
	
	public void printDict();
	
	public void setFileReaderImpl(Class<? extends FileReader> readerImpl);
}
