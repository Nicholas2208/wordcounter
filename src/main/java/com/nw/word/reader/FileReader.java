package com.nw.word.reader;

import java.io.IOException;

public interface FileReader {
	
	public boolean hasNext() throws IOException;
	
	public String nextChunk(); 
	
	public String[] nextChunkInWords();
	
    public void setFileInputStreamIfNull(String path);
	
	public void close();

}
